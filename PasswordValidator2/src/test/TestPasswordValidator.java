package test;

import static org.junit.Assert.*;

import org.junit.Test;

import Password.PasswordValidator;

/*
 * @author Vishwa Patel
 * SID: 991516280
 * 
 * Validates passwords using TDD
 * Requirements: #1 - check for length, length must be 8 or more than 8 character
 * 				 #2 - check if password contains two or more then two digits	
 *  * */
public class TestPasswordValidator {
		
	@Test
	public void testHasValidCaseCharsRaguler()
	{
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("aBAmMsiB"));
		//fail("Invalid case chars");
	}	

	@Test
	public void testHasValidCaseCharsBoundryIn()
	{
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("jL"));
		//fail("Invalid case chars");
	}	
	
	@Test
	public void testHasValidCaseCharsException()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("126378"));
		//fail("Invalid case chars");
	}	

	@Test
	public void testHasValidCaseCharsExceptionSpecial()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("%^!$@$"));
		//fail("Invalid case chars");
	}	

	@Test
	public void testHasValidCaseCharsExceptionBlank()
	{
		assertFalse ("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
		//fail("Invalid case chars");
	}	

	@Test
	public void testHasValidCaseCharsExceptionNull()
	{
		assertFalse ("Invalid case chars", PasswordValidator.hasValidCaseChars( null ));
		//fail("Invalid case chars");
	}	

	@Test
	public void testHasValidCaseCharsBoundryOutUpper()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("BKILBY"));
		//fail("Invalid case chars");
	}	
	
	@Test
	public void testHasValidCaseCharsBoundryOutLower()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("qnnbsk"));
		//fail("Invalid case chars");
	}	
	
	
	
	//////////////////////////////////
	
	
//	
//	@Test
//	public void testHasValidCaseChars()
//	{
//		fail("Invalid case chars");
//	}	
//	
//	
//	///////////////////////////////////////////////////////
//	
//	@Test
//	public void testIsvalidLengthRegular() {
//		boolean result = PasswordValidator.isValidLength("1234567890");
//		assertTrue("Invalid length", result);
////		
//	}
//	
//	@Test
//	public void testIsvalidLengthException() {
//		boolean result = PasswordValidator.isValidLength( "" );
//		assertFalse("Invalid length", result);
//	}
//	
//	//step no 11 a
//	@Test
//	public void testIsvalidLengthExceptionSpaces() {
//		boolean result = PasswordValidator.isValidLength("    t e st   ");
//		assertFalse("Invalid length", result);
//	}
//	
//	@Test
//	public void testIsvalidLengthBoundryIn() {
//		boolean result = PasswordValidator.isValidLength( "12345678" );
//		assertTrue("Invalid length", result);
//	}
//	
//	@Test
//	public void testIsvalidLengthBoundryOut() {
//		boolean result = PasswordValidator.isValidLength( "123456789" );
//		assertTrue("Invalid length", result);
////		fail("Invalid length");
//	}
//	
//////	--------------------------------------------------------------------
//	@Test
//	public void testIsDigitRegular() {
//		boolean result = PasswordValidator.isDigit("Vishwapatel123");
//		assertTrue("Invalid length", result);
//	}
//	
//	@Test
//	public void testIsDigitException() {
//		boolean result = PasswordValidator.isDigit("vishwapatel");
//		assertFalse("Invalid length", result);
//	}
//	
//	@Test
//	public void testIsDigitBoundryIn() {
//		boolean result = PasswordValidator.isDigit("Vishwa12");
//		assertTrue("Invalid length", result);
//	}
//	
//	@Test
//	public void testIsDigitBoundryOut() {
//		boolean result = PasswordValidator.isDigit("1234567890");
//		assertTrue("Invalid length", result);
//	}

}




























