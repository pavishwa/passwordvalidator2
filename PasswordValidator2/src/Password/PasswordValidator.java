package Password;

import javax.swing.JOptionPane;

/*
 * @author Vishwa Patel
 * SID: 991516280
 * 
 * The class creates method to validate passwords against the criteria.
 * 1. isValidLength checks if password is 8 characters long without any spaces.
 * 2. isDigit checks for total digits that two digit must be in password.  
 * */
public class PasswordValidator {
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGIT = 2;
	
	
public static boolean hasValidCaseChars( String password) {
		
		return password != null && password.matches( "^.*[a-z].*$" ) &&
				password.matches("^.*[A-Z].*$");
		
	}
	
	public static boolean isValidLength(String password) {
		
		return password.indexOf(" ")<0 && password.length() >= MIN_LENGTH;
		
	}
	
	public static boolean isDigit(String password) {
		int count = 0;
		for(int i=0; i< password.length(); i++) {
			if(Character.isDigit(password.charAt(i))) {
				count++;
			}
		}
		if(count >= MIN_DIGIT) {
			return true;
		}
		return false;
    }
}
